# -*- coding: UTF-8 -*-

from flask import Flask, request, render_template, session, redirect
from flask_babel import Babel, gettext
from datetime import datetime
import settings
from rules import rules_romanizations
from flask_cors import CORS, cross_origin

app = Flask(__name__)

def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'be')

babel = Babel(app, locale_selector=get_locale)

# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sdfsdfsfdgdfg4fsd',
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


def run(form_options, lang):
    tpl = """{}"""
    output_text = tpl.format(form_options['input'])
    return output_text


@app.route('/', methods=['GET', 'POST'])
def ServiceRomanizator():
    lang = get_locale()
    form_options = {}
    _input = gettext('default input').replace('\\n', '\n')
    is_post = False
    output_text = ""

    if request.method == "POST":
        is_post = True
        _input = request.form.get("inputText")
        romanized = []
        options = request.form.get("mode")
        rules_romanizations(_input, romanized, options)
        form_options['input'] = "".join(romanized)
        form_options['radio'] = options
        
        output_text = run(form_options, lang)
    return render_template('index.html', is_post=is_post, form_options=form_options, output_text=output_text,
                           _input=_input, lang=lang, now=datetime.utcnow())


cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/api', methods=['POST'])
@cross_origin()
def api_romanize():
    data = request.get_json()
    _input = data.get('input')
    options = data.get('options')
    romanized = []
    rules_romanizations(_input, romanized, options)
    output_text = "".join(romanized)
    return {'output_text': output_text}

if __name__ == '__main__':
    app.run(use_reloader=True, debug=True, host=settings.host, port=settings.port)
