from dictionary import letters, letters_two


def rules_romanizations(text:str, empty_list:list, radiobutton:str):
    """Returns romanized text, taking into account the rules of the Belarusian language
    rules_first = Some vowels at the beginning of a word, after vowels, an apostrophe, a dividing soft sign, and "Ў"
    rules_second = Some vowels after consonants
    rules_third = Some vowels after hard letter "Л"
    rules_fourth = Some consonants before a soft sign
    rules_fifth = Hard letter L rule (only classic romanizator)
    """
    rules_first = {'А','а','Е','е','Ё','ё','І','і','О','о','У','у','Ў','ў','Э','э','Ю','ю','Я','я','Ь','ь',"'", " ", "\r", "\n"}
    rules_second = {" ", "\r", "\n"}
    rules_third = {"Л", "л"}
    rules_fourth = {'Ь','ь'}
    rules_fifth = {'Е','е','Ё','ё','І','і','Ю','ю','Я','я','Ь','ь'}

    if radiobutton == "radiobutton3":
        for i, letter in enumerate(list(text)):

            last_char = text[i-1] if i>0 else None

            if letter == "е":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters_two.get(letter, letter))
                else:
                    empty_list.append("ie")     
            elif letter == "ё":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters_two.get(letter, letter))
                else:
                    empty_list.append("io")
            elif letter == "ю":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters_two.get(letter, letter))
                else:
                    empty_list.append("iu")
            elif letter == "я":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters_two.get(letter, letter))
                else:
                    empty_list.append("ia")
            else:        
                empty_list.append(letters_two.get(letter, letter))
        return empty_list
    else:
        for i, letter in enumerate(list(text)):

            last_char = text[i-1] if i>0 else None
            if i == len(text) - 1:
                next_char = None
            else:
                next_char = text[i+1]

            if letter == "е":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters.get(letter, letter))
                elif last_char not in rules_second:
                    empty_list.append("ie")
                elif last_char in rules_third:
                    empty_list.append("e")      
            elif letter == "ё":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters.get(letter, letter))
                elif last_char not in rules_second:
                    empty_list.append("io")
                elif last_char in rules_third:
                    empty_list.append("o")
            elif letter == "ю":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters.get(letter, letter))
                elif last_char not in rules_second:
                    empty_list.append("iu")
                elif last_char in rules_third:
                    empty_list.append("u")
            elif letter == "я":
                if last_char in rules_first or last_char is None:
                    empty_list.append(letters.get(letter, letter))
                elif last_char not in rules_second:
                    empty_list.append("ia")
                elif last_char in rules_third:
                    empty_list.append("a")
            elif letter == "З":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("Ź")
            elif letter == "з":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("ź")
            elif letter == "Л":
                if radiobutton == "radiobutton1":
                    if next_char not in rules_fourth or next_char is None:
                        empty_list.append(letters.get(letter, letter))   
                    else:
                        empty_list.append("Ĺ")
                elif radiobutton == "radiobutton2":
                    if next_char not in rules_fifth or next_char is None:
                        empty_list.append("Ł")   
                    else:
                        empty_list.append("L")        
            elif letter == "л":
                if radiobutton == "radiobutton1":
                    if next_char not in rules_fourth or next_char is None:
                        empty_list.append(letters.get(letter, letter))   
                    else:
                        empty_list.append("ĺ")
                if radiobutton == "radiobutton2":
                    if next_char not in rules_fifth or next_char is None:
                        empty_list.append("ł")   
                    else:
                        empty_list.append("l")    
            elif letter == "С":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("Ś")
            elif letter == "с":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("ś")
            elif letter == "Ц":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("Ć")
            elif letter == "ц":
                if next_char not in rules_fourth or next_char is None:
                    empty_list.append(letters.get(letter, letter))   
                else:
                    empty_list.append("ć")                                                                                       
            else:
                empty_list.append(letters.get(letter, letter))
        return empty_list
        